import { Injectable } from "@angular/core";
import { CompileShallowModuleMetadata } from "@angular/compiler";

@Injectable({ providedIn: "root" })
export class HeroesService {
   private heroes: Heroe[] = [
      {
         nombre:"Aquaman",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/aquaman.png",
         aparicion: "1941-11-01",
         casa: "DC"
      },
      {
         nombre:"Batman",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/batman.png",
         aparicion: "1939-05-01",
         casa: "DC"
      },
      {
         nombre:"Daredevil",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/daredevil.png",
         aparicion: "1964-01-01",
         casa: "Marvel"
      },
      {
         nombre:"Hulk",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/hulk.png",
         aparicion: "1962-05-01",
         casa: "Marvel"
      },
      {
         nombre:"Linterna Verde",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/linterna-verde.png",
         aparicion: "1940-06-01",
         casa: "DC"
      },
      {
         nombre:"Spider-Man",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/spiderman.png",
         aparicion: "1962-08-01",
         casa: "Marvel"
      },
      {
         nombre:"Wolverine",
         bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         img: "assets/img/wolverine.png",
         aparicion: "1974-11-01",
         casa: "Marvel"
      },
   ];

   constructor() {
      console.log("Servicio listo");
   }

   getHeroes(): Heroe[] {
      return this.heroes
   }
   
   getHeroe(idx: string) {
      return this.heroes[idx]
   }
}

export interface Heroe {
   nombre: string,
   bio: string,
   img: string,
   aparicion: string,
   casa: string
}