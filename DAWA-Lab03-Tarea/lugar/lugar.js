// Prueba E
const axios = require("axios")

const getLugarLatLng = async (dir) => {
    const encodeUrl = encodeURI(dir);

// 	En la utlima PARTE sp, es

const instance = axios.create({
    baseURL:
// Grados Celcius
    `http://api.openweathermap.org/data/2.5/weather?q=${encodeUrl}&units=metric&appid=8a6ddedb03aba0160e0b2d627d0de741`,
// Emlace Nueva 
 //   `http://api.openweathermap.org/data/2.5/weather?q=${encodeUrl},${encodeUrlre}&units=metric&appid=8a6ddedb03aba0160e0b2d627d0de741`,

    
// Enlace de ejemplo
    //http://api.openweathermap.org/data/2.5/weather?q=Peru,arequipa&units=metric&appid=8a6ddedb03aba0160e0b2d627d0de741 
});

    const resp = await instance.get();

    if (resp.data.length ===0 ){
        throw new Error(`No hay resultados para ${dir}`);
    }
    const Titulo = 'Lista de los datos importantes del Clima ';
    const data = resp.data ;  // Imprime todos los datos 
    const codigo = data.cod;
    const direccion = data.name;
    
    const  temperatura = data.main.temp + "Cº";
    const  temperaturaMAX = data.main. temp_max + "Cº";
    const  temperaturaMIN = data.main.temp_min + "Cº";
    const presion = data.main.pressure;
    // Como se ve
    const clima = data.weather;

    return { 
        Titulo,
        direccion,
        codigo,
        temperatura,
        temperaturaMAX,
        temperaturaMIN,
        presion,
        clima
    };
};

module.exports = {
    getLugarLatLng
}
