const path = require('path') // N1
const express = require('express')  //  n1
const mongoose = require('mongoose')
const dotenv = require('dotenv') // N1
const morgan = require('morgan')  // N1
const exphbs = require('express-handlebars')   // N1
const methodOverride = require('method-override')
const passport = require('passport')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const connectDB = require('./config/db')  // N1

// Load config
dotenv.config({ path: './config/config.env' })  //N1

// Passport config
require('./config/passport')(passport)

//Base de datos que requiere config db
connectDB()   //N1

const app = express() //n1

// Body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// Method override
app.use(
  methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      let method = req.body._method
      delete req.body._method
      return method
    }
  })
)

// Logging process de morgan      -------N1
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

// Handlebars Helpers
const {
  formatDate,
  stripTags,
  truncate,
  editIcon,
  select,
} = require('./helpers/hbs')

// Handlebars  -------------------------------n1
app.engine(
  '.hbs',
  exphbs({
    helpers: {
      formatDate,
      stripTags,
      truncate,
      editIcon,
      select,
    },
    defaultLayout: 'main',
    extname: '.hbs',
  })
)
app.set('view engine', '.hbs')





// Sessions
app.use(
  session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
)

// Passport middleware
app.use(passport.initialize())
app.use(passport.session())

// Set global var
app.use(function (req, res, next) {
  res.locals.user = req.user || null
  next()
})

// Static folder reconecimiento de css  --------------------n1
app.use(express.static(path.join(__dirname, 'public')))

// Routes ----------------------------------------------- N1
app.use('/', require('./routes/index'))
app.use('/auth', require('./routes/auth'))
app.use('/stories', require('./routes/stories'))


// Ejecucion de puerto 3000
const PORT = process.env.PORT || 3000 // n1

app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)   //n1
)


// Elementos para registro manual
const flash = require('connect-flash');

// Inicializacion
require('./passport/local-auth');



app.use(flash());


app.use((req, res, next) => {
  app.locals.signinMessage = req.flash('signinMessage');
  app.locals.signupMessage = req.flash('signupMessage');
  app.locals.user = req.user;
  console.log(app.locals)
  next();
});

