const mongoose = require('mongoose'); //2
 const { Schema } = mongoose; //2

 // Esquema de como lucir los datos
const NoteSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    date: {
      type: Date,
      default: Date.now
    }
  },


);
//2

module.exports =mongoose.model("Note", NoteSchema);
//2