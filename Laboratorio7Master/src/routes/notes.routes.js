// Url rutas de mi servidor - Crear - Eliminar Actualizae

const express = require("express");  //2  1.00.19
const router = express.Router();   //2

// Requiere elementos de models
const Note = require('../models/Note'); // 2


//Nuevo
router.get("/notes/add", (req , res) =>  {
  res.render('notes/new-note');
});  //2

// Recibe los datos
router.post("/notes/new-note", async (req , res) =>  {
 const { title , description} = req.body;
 const errors = [];
 
if (!title) {
  errors.push({text: 'Por favor Ingresar Titulo'});
  
}
if (!description) {
  errors.push({text: 'Por favor Ingresar Descripcion'});
  
}
if (errors.length > 0) {
  res.render('notes/new-note' , {
    errors,
    title,
    description
  });

  
}else{
 // Prueba para consola console.log(req.body);
 const newNote = new Note({ title, description });
await newNote.save();  // Guarda el proceso en la bd

 res.redirect('/notes') ; // redirecciona a otra ruta
}

});  //2

// Listado de elementos
router.get('/notes', async (req, res) => {
  await Note.find().sort({date:'desc'})
    .then(documentos => {
      const contexto = {
          notes: documentos.map(documento => {
          return {
              title: documento.title,
              description: documento.description
          }
        })
      }
      res.render('notes/all-notes', {notes: contexto.notes }) 
    })
})


// Zona de editado  1.38   //1


// redirigir dato

router.get("/notes/edit", (req , res) =>  {
  res.render('notes/edit-note');
});  //2

/*

router.get('/notes/editar/:id', async (req, res) => {
  const dataNota = await Note.findById(req.params.id).lean();
 
  res.render('notes/nota-edit', {dataNota});
});


router.get('/edit/:id', async (req, res) => {
  const { id } = req.params;
  const task = await Task.findById(id);
  res.render('edit', { 
    task });
}); 
*/

module.exports = router; //2
