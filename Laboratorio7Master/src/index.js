const express = require('express');    // 2
const exphbs = require('express-handlebars'); //2
const path = require('path'); //2
const methodOverride = require('method-override'); //2
const session = require('express-session');  //2

const Handlebars = require('handlebars');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');

// Inicializacion
const app = express();  //2
require('./database');  //2

// Ajustes  ------- 1
app.set('port', process.env.PORT || 4000);  // 2
app.set('views', path.join(__dirname, 'views')); // 2

app.engine('.hbs', exphbs({
  handlebars: allowInsecurePrototypeAccess(Handlebars),
  defaultLayout: 'main',
  layoutsDir: path.join(app.get('views'), 'layouts'),
  partialsDir: path.join(app.get('views'), 'partials'),
  extname: '.hbs'
}));     //2                           //1



// Se congigura el motor de las vistas
app.set('view engine', '.hbs'); //2

// middlewares --------1
app.use(express.urlencoded({extended: false}));  //2
app.use(methodOverride('_method'));  //2

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
})); //2

/*
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

*/


// llamado de routes
app.use(require('./routes/index.routes')); //2
app.use(require('./routes/users.routes')); //2
app.use(require('./routes/notes.routes')); //2

// static archivos
app.use(express.static(path.join(__dirname, 'public')));  //2

// Area de puerto de escucha   ----1
app.listen(app.get('port'), () => {
    console.log('Usted esta en el ´puerto ', app.get('port'));
  });  //2