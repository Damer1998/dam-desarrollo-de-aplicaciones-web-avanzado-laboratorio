// Contruccion de la coleccion task  -- Boletos de peliculas
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = Schema({
  nombre: String,
  ver: String,
  genero: String,
  precio: String,
  estreno: String,
  entrada: String,
  status: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('tasks', TaskSchema);