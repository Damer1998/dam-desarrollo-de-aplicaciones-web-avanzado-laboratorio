const router = require('express').Router();
const passport = require('passport');

router.get('/', (req, res, next) => {
  res.render('index');
});

router.get('/signup', (req, res, next) => {
  res.render('signup');
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/profile',
  failureRedirect: '/signup',
  failureFlash: true
})); 

router.get('/signin', (req, res, next) => {
  res.render('signin');
});


router.post('/signin', passport.authenticate('local-signin', {
  successRedirect: '/profile',
  failureRedirect: '/signin',
  failureFlash: true
}));

router.get('/profile',isAuthenticated, (req, res, next) => {
  res.render('profile');
});

router.get('/logout', (req, res, next) => {
  req.logout();
  res.redirect('/');
});


function isAuthenticated(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }

  res.redirect('/')
}


// Seccion de Task Boletos

const Task = require('../models/task');

router.get('/peliculas', async (req, res) => {
  const tasks = await Task.find();
  console.log(tasks);
  res.render('peliculas', {
    tasks
  });
});


// A
router.post('/add', async (req, res) => {
  const task = new Task(req.body);
  await task.save();
   res.redirect('/peliculas');
});



router.get('/delete/:id', async (req, res ) => {
  let { id } = req.params;
  await Task.remove({_id: id});
  res.redirect('/peliculas');
});



router.get('/turn/:id', async (req, res) => {
  let { id } = req.params;
  const task = await Task.findById(id);
  task.status = !task.status;
  await task.save();
  res.redirect('/peliculas');
});


router.get('/edit/:id', async (req, res) => {
  const { id } = req.params;
  const task = await Task.findById(id);
  res.render('edit', { 
    task });
}); 


router.post('/edit/:id', async (req, res) => {
  const { id } = req.params;
  await Task.update({_id: id}, req.body);
  res.redirect('/peliculas');
});



// Seccionde Peliculas en estreno


const path = require('path');
const { unlink } = require('fs-extra');


// Models
const Image = require('../models/Image');

router.get('/estreno', async (req, res) => {
    const images = await Image.find();
    res.render('estreno', { images });
});



router.get('/upload', (req, res) => {
    res.render('upload');
});

router.post('/upload', async (req, res) => {
    const image = new Image();
    image.title = req.body.title;
    image.description = req.body.description;
    image.filename = req.file.filename;
    image.path = '/img/uploads/' + req.file.filename;
    image.originalname = req.file.originalname;
    image.mimetype = req.file.mimetype;
    image.size = req.file.size;

    await image.save();
    res.redirect('/estreno');
});

router.get('/image/:id', async (req, res) => {
    const { id } = req.params;
    const image = await Image.findById(id);
    res.render('perfilpelicula', { image });
});

router.get('/image/:id/delete', async (req, res) => {
    const { id } = req.params;
    const imageDeleted = await Image.findByIdAndDelete(id);
    await unlink(path.resolve('./src/public' + imageDeleted.path));
    res.redirect('/estreno');
});




module.exports = router;
