// Area de control
const express = require('express');
const path = require('path');
const engine = require('ejs-mate');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const morgan = require('morgan');

// Imagenes 
const multer = require('multer');
const uuid = require('uuid/v4');
const { format } = require('timeago.js');




// Inicializacion
const app = express();
require('./database');
require('./passport/local-auth');

// Ajustes elementos a reconocer  en views el ejs
app.set('views', path.join(__dirname, 'views'))
app.engine('ejs', engine);
app.set('view engine', 'ejs');

// Ajuste elementos a reconocer en public  img   carpetas estaticas

app.use(express.static(path.join(__dirname, 'public'))); 


// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

app.use(session({
  secret: 'mysecretsession',    // area de seccion
  resave: false,
  saveUninitialized: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  app.locals.signinMessage = req.flash('signinMessage');
  app.locals.signupMessage = req.flash('signupMessage');
  app.locals.user = req.user;
  console.log(app.locals)
  next();
});

// middlewars multer
const storage = multer.diskStorage({
  destination: path.join(__dirname, 'public/img/uploads'),
  filename: (req, file, cb, filename) => {
      console.log(file);
      cb(null, uuid() + path.extname(file.originalname));
  }
}) 
app.use(multer({storage}).single('image'));

// Global variables
app.use((req, res, next) => {
  app.locals.format = format;
  next();
});



// routes
app.use('/', require('./routes/index'));


//LIsta de rutas
app.get("/agregar", function(req,res){
  res.render('agregar' )
});


app.get("/peliculas", function(req,res){
  res.render('peliculas' )
});


app.get("/estreno", function(req,res){
  res.render('estreno' )
});

app.get("/perfilpelicula", function(req,res){
  res.render('perfilpelicula' )
});





// Iniciando secicon
app.set('port', process.env.PORT || 4000);
app.listen(app.get('port'), () => {
  console.log(`Usted esta en el puerto ${app.get('port')}`);
});