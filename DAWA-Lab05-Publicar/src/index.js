
// Establece el valor del año de un objeto
Date.prototype.getDoY = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.floor(((this - onejan) / 86400000) + 1);
};

function getAge(cumpledato) {
    function salto(año) {
        return año % 4 == 0 && (año % 100 != 0 || año % 400 == 0);
    }

    var ahora = new Date(),
    Edad = ahora.getFullYear() - cumpledato.getFullYear(),
    formula = ahora.getDoY(),
        doycumple = cumpledato.getDoY();

    // Permite normalizar el dia del año en los años bisiestos
    if (salto(ahora.getFullYear()) && formula > 58 && doycumple > 59)
    formula--;

    if (salto(cumpledato.getFullYear()) && formula > 58 && doycumple > 59)
    doycumple--;

    if (formula <= doycumple)
    Edad--;  // birthday not yet passed this year, so -1

    return Edad;
};

// El cumpleaños real
var nacimiento = new Date(1998, 13, 12);
console.log(getAge(nacimiento));
