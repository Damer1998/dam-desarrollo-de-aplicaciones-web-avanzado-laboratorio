const express = require("express");

const app = express();

const port = process.env.PORT || 3000


app.use(express.static(__dirname + '/public'));

app.set('view engine','pug');

// LLamado de index
app.get("/", function (req, res){
    res.render('index')
});



app.listen(port, () => console.log(`escuchando peticiones en puerto ${port}`));