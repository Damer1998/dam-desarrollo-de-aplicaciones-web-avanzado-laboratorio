// Sturae reconoce como el seccion de task - estructura del ingreso
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const rolesValidos = {
    values: [
        'ADMIN_ROLE',
        'USER_ROLE'
    ],
    message:  '{VALUE} no es un rol valido'

}

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, "El nombre es necesario"],
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El correo es necesario"],
    },
    password: {
        type: String,
        required: [true, "El password es necesario"],
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos

    },
    estado: {
        type: Boolean,
        default: true,

    },
});

module.exports = mongoose.model('Usuario', usuarioSchema)